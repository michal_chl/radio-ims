/**
 * Author: Jakub Ostrowski
 */
package debug {

import com.junkbyte.console.ConsoleChannel;

public class ConsoleChannels {
    public static const DEBUG:ConsoleChannel = new ConsoleChannel("debug");

    public static const APPLICATION:ConsoleChannel = new ConsoleChannel("application");
    public static const LOADER_MAX:ConsoleChannel = new ConsoleChannel("loader_max");

    public static const NEWS:ConsoleChannel = new ConsoleChannel("news");
    public static const PROMOTIONS:ConsoleChannel = new ConsoleChannel("promotions");
    public static const SHOPS:ConsoleChannel = new ConsoleChannel("shops");
    public static const PATH:ConsoleChannel = new ConsoleChannel("path");
    public static const SERVICES:ConsoleChannel = new ConsoleChannel("services");

    public static const MONITOR_SERVICE:ConsoleChannel = new ConsoleChannel("monior_service");

    public static const MAP:ConsoleChannel = new ConsoleChannel("map");
    public static const MENU_PANEL:ConsoleChannel = new ConsoleChannel("menuPanel");
    public static const LEGEND:ConsoleChannel = new ConsoleChannel("legend");
    public static const NAVIGATION:ConsoleChannel = new ConsoleChannel("navigation");

    public static const PUBLIC_TRANSPORT:ConsoleChannel = new ConsoleChannel("publicTransport");

    public static const TRACKING:ConsoleChannel = new ConsoleChannel("tracking");
    public static const LOGGING:ConsoleChannel = new ConsoleChannel("logging");
}
}