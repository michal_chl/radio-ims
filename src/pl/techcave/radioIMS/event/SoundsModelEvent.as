/**
 * Created by Michał Chlebowski on 2015-02-05.
 */
package pl.techcave.radioIMS.event {
import flash.events.Event;

public class SoundsModelEvent extends Event {

    public static const PLAYLISTS_COMPLETE:String = "SounsModelEvent.PLAYLIST_COMPLETE";

    public function SoundsModelEvent( type:String ) {

        super ( type, false, false );

    }

}
}
