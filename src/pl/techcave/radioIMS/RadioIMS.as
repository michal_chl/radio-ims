﻿package pl.techcave.radioIMS {

import com.greensock.events.LoaderEvent;
import com.greensock.loading.ImageLoader;
import com.greensock.loading.LoaderMax;
import com.junkbyte.console.Cc;
import debug.ConsoleChannels;

import flash.display.Loader;

import flash.display.Shape;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.media.Sound;
import flash.net.URLLoader;
import flash.net.URLRequest;

import pl.techcave.radioIMS.model.SoundsModel;
import pl.techcave.radioIMS.model.vo.PlaylistVO;

import pl.techcave.radioIMS.model.vo.SoundVO;
import pl.techcave.radioIMS.php.PhpApi;
import pl.techcave.radioIMS.view.Player;
import pl.techcave.radioIMS.view.Preloader;

[SWF(width="1920", height="1080", frameRate="60", backgroundColor="0xffffff")]
public class RadioIMS extends Sprite {

        private static const PLAYLISTS_TAG:String = "<playlists>";

        private var loader:LoaderMax;
        //private var loader:Loader;

        public function RadioIMS() {

            // constructor code
            Cc.startOnStage(this.stage, "]" );

            ConsoleChannels.APPLICATION.log( " RadioIMS() " );

            setupPlaylist();

            setupPreloader();

            loadPlaylistData();
        }

        private var _player:Player;
        private function setupPlaylist():void{
            _player = new Player();
            addChild(_player);
        }

        private var _preloader:Preloader;
        private function setupPreloader():void{
            _preloader = new Preloader();
            addChild(_preloader);
        }

        private function loadPlaylistData():void {

            //http://89.171.130.104/dev_radio.techcave.pl/test/members.php?do=new
            var urlRequest:URLRequest = new URLRequest();
            //urlRequest.url = PhpApi.getPlaylistURL( 2622, 1 );
            //urlRequest.url = "http://89.171.130.104/dev_radio.techcave.pl/test/members.php?do=new";
            urlRequest.url = "../data/playlists.xml";

            var urlLoader:URLLoader = new URLLoader();
            //urlLoader.addEventListener(Event.COMPLETE, onPlaylistXMLComplete);
            urlLoader.addEventListener(Event.COMPLETE, onPlaylistsListComplete);
            urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
            urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
            urlLoader.load( urlRequest );

        }

        private var playlistsVector:Vector.<PlaylistVO> = new Vector.<PlaylistVO>();

        private function onPlaylistsListComplete( e:Event ) : void {

            trace(" onPlaylistsListComplete() " );

            loader = new LoaderMax( { name: 'imagesLoader', onComplete: onPlaylistsImagesComplete, onError: onPlaylistsImagesError } );

            loader.addEventListener( LoaderEvent.COMPLETE, onPlaylistsImagesComplete );
            loader.addEventListener( LoaderEvent.ERROR, onPlaylistsImagesError );
            loader.addEventListener( LoaderEvent.SECURITY_ERROR, onPlaylistsImagesSecurityError );
            loader.addEventListener( LoaderEvent.PROGRESS , onPlaylistsImagesProgress );

            var dataStr:String = ( e.currentTarget as URLLoader ).data.toString();
            var id:int = dataStr.indexOf( PLAYLISTS_TAG );
            dataStr = dataStr.slice( id, dataStr.length );

            var dataXML:XML = XML(dataStr);
            var playlistVO:PlaylistVO;

            //var dataDomain:String = "http://89.171.130.104/dev_radio.techcave.pl/test/";
            var dataDomain:String = "http://radio.techcave.pl/test2/";

            //imageQueue = new LoaderMax({name: 'promotionsImageQueue', onComplete: onCompleteHandler, onError: onErrorHandler});

            for ( var i:uint = 0; i < dataXML.playlist.length(); i++ ) {

                playlistVO = PlaylistVO.fromXML( dataXML.playlist[i] );

                playlistsVector.push( playlistVO );

                loader.append(new ImageLoader( dataDomain + playlistVO.imageURL, {name: playlistVO.imageURL  }));

                //SoundsModel.instance.playlistVector.push( playlistVO );
            }

            loader.load();
        }

        private function onPlaylistsImagesComplete( e:LoaderEvent ):void{

            trace("onPlaylistsImagesComplete ", e.text, e.data );

            for each ( var playlistVO in playlistsVector ) {
                playlistVO.image = loader.getContent( playlistVO.imageURL );
            }

            SoundsModel.instance.playlistVector = playlistsVector;

            _preloader.visible = false;
        }

        private function onPlaylistsImagesError( e:LoaderEvent ):void{
            trace("onPlaylistsImagesError", e.text, e.data );
        }

        private function onPlaylistsImagesSecurityError( e:LoaderEvent ):void{
            trace("onPlaylistsImagesSecurityError", e.text, e.data );
        }

        private function onPlaylistsImagesProgress( e:LoaderEvent ):void{
            //trace( "progress = ", loader.progress, loader.rawProgress, loader.bytesLoaded, loader.bytesTotal );
            _preloader.progress = loader.progress;
        }

/*
        private var _loadedCounter:uint = 0;
        private var _totalToLoadCount:uint = 0;
        private function onPlaylistXMLComplete(e:Event):void {
            ConsoleChannels.APPLICATION.log( "onPlaylistXMLComplete: " + ( e.currentTarget as URLLoader ).data );

            var xml:XML = XML( ( e.currentTarget as URLLoader ).data );
            var songXML:XML;
            var soundVO:SoundVO;

            _totalToLoadCount = xml.song.length();

            for ( var i:uint = 0; i < _totalToLoadCount; i++ ) {

                songXML = xml.song[i];
                soundVO = SoundVO.fromXML( songXML );

                soundVO.sound = new Sound( new URLRequest( songXML.@link + songXML.@url ) );
                soundVO.sound.addEventListener(Event.COMPLETE, onSoundComplete);

                SoundsModel.instance.soundsVOVector.push( soundVO );
            }
        }

        private function onSoundComplete(e:Event):void {
            _loadedCounter++;
            if ( _loadedCounter == _totalToLoadCount ) {
                ConsoleChannels.APPLICATION.log(" onSoundComplete() " + SoundsModel.instance.soundsVOVector.length);
            }
        }
*/


        private function onIOError(e:IOErrorEvent):void {
            ConsoleChannels.APPLICATION.log("onIOError " + e.text);
        }

        private function onSecurityError(e:SecurityErrorEvent):void {
            ConsoleChannels.APPLICATION.log("onSecurityError " + e.text);
        }

    }

}