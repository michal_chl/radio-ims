/**
 * Created by Michał Chlebowski on 2015-01-30.
 */
package pl.techcave.radioIMS.view {

import assets.PlayerAsset;

import flash.display.Sprite;

import flash.events.MouseEvent;

import pl.techcave.radioIMS.event.SoundsModelEvent;

import pl.techcave.radioIMS.model.SoundsModel;
import pl.techcave.radioIMS.model.vo.PlaylistVO;

import pl.techcave.radioIMS.view.ui.MenuButton;
import pl.techcave.radioIMS.view.ui.PlaylistItem;

public class Player extends PlayerAsset {

    private static const COLS_COUNT:uint = 4;
    private static const SPACING:uint = 97;

    private var _currentBtn:MenuButton;


    public function Player() {

        SoundsModel.instance.addEventListener( SoundsModelEvent.PLAYLISTS_COMPLETE, onPlaylistsComplete );

        var buttonsVector:Vector.<MenuButton> = new <MenuButton>[ new MenuButton( newPlaylistBtn ), new MenuButton( archivedPlaylistBtn ) ];
        var buttonsNames:Vector.<String> = new <String>["New playlists", "Archived playlists"];

        for ( var i:uint = 0; i < buttonsVector.length; i++ ){

            buttonsVector[i].label = buttonsNames[i];
            buttonsVector[i].iconFrame = (i + 1);
            buttonsVector[i].addEventListener( MouseEvent.CLICK, onMenuBtnClick );
        }

        _currentBtn = buttonsVector[0];
        _currentBtn.select();
    }

    private function onMenuBtnClick(e:MouseEvent):void {

        if (_currentBtn) {
            _currentBtn.unselect();
        }

        _currentBtn = e.currentTarget as MenuButton;
        _currentBtn.select();

/*
        switch ( e.currentTarget ){

            case asset.newPlaylistBtn:
                break;

            case asset.archivedPlaylistBtn:
                break;

            default:
                break;
        }
*/

    }

    private var _playlistsContainer:Sprite;
    private function onPlaylistsComplete(e:SoundsModelEvent):void {

        var playlistsVector:Vector.<PlaylistVO> = ( e.currentTarget as SoundsModel ).playlistVector;

        var playlistItem:PlaylistItem;

        _playlistsContainer = new Sprite();

        var xx:Number = 0;
        var yy:Number = 0;

        for ( var i:uint = 0; i < playlistsVector.length; i++ ){

            playlistItem = new PlaylistItem( playlistsVector[i] );



            if ( i > 0 && i % COLS_COUNT == 0 ) {
                yy += playlistItem.height + SPACING;
                xx = 0;
            }
            playlistItem.x = xx;
            playlistItem.y = yy;
            xx += playlistItem.width + SPACING;

            //playlistItem.image = playlistsVector[i].image;

            _playlistsContainer.addChild( playlistItem );
        }

        sp.source = _playlistsContainer;

    }

}
}
