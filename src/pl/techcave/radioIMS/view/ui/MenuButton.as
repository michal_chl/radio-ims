/**
 * Created by Michał Chlebowski on 2015-02-04.
 */
package pl.techcave.radioIMS.view.ui {

import assets.MenuButtonAsset;

import flash.display.MovieClip;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;

//public class MenuButton extends MenuButtonAsset {
public class MenuButton extends EventDispatcher  {

    private var _asset:MenuButtonAsset;

    public function MenuButton( asset:MenuButtonAsset ) {

        _asset = asset;

        _asset.bg.stop();
        _asset.tf.mouseEnabled = false;
        _asset.buttonMode = true;

        _asset.addEventListener(MouseEvent.CLICK, dispatchEvent);
    }

/*
    public function get asset():MenuButtonAsset{
        return _asset;
    }
*/

    public function set label(value:String):void{
        _asset.tf.text = value;
    }

    public function set iconFrame(value:uint):void{
        _asset.icon.gotoAndStop(value);
    }

    public function select():void{
        _asset.bg.gotoAndStop(2);
    }

    public function unselect():void{
        _asset.bg.gotoAndStop(1);
    }


}
}
