/**
 * Created by Michał Chlebowski on 2015-02-05.
 */
package pl.techcave.radioIMS.view.ui {

import com.greensock.loading.display.ContentDisplay;

import debug.ConsoleChannels;

import pl.techcave.radioIMS.model.vo.PlaylistVO;
import assets.PlaylistItemAsset;

public class PlaylistItem extends PlaylistItemAsset {

    public function PlaylistItem( playlistVO : PlaylistVO ) {

        ConsoleChannels.APPLICATION.log( "PlaylistItem playlistVO = " + playlistVO.name + "  " + playlistVO.imageURL );

        var image:ContentDisplay = playlistVO.image;

        image.width = 100;
        image.scaleY = image.scaleX;
        addChild( image );

    }

}
}
