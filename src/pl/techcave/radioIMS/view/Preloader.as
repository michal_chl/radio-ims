/**
 * Created by Michał Chlebowski on 2015-02-17.
 */
package pl.techcave.radioIMS.view {
import assets.PreloaderAsset;

import flash.display.Sprite;

public class Preloader extends PreloaderAsset{

    public function Preloader() {
        stripe.scaleX = 0;
    }

    public function set progress(value:Number):void{
        stripe.scaleX = value;
    }

}
}
