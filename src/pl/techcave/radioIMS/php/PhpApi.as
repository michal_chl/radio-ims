/**
 * Created by Michał Chlebowski on 2015-01-12.
 */
package pl.techcave.radioIMS.php {

    public class PhpApi {

        //http://89.171.130.104/dev_radio.techcave.pl/test/members.php?do=new

        //private static const APP_URL:String = "http://radio.techcave.pl/";
        private static const APP_URL:String = "http://89.171.130.104/dev_radio.techcave.pl/";

        private static const PLAYLISTS_DIR:String = "test/html/";

        private static const GET_PLAYLIST:String = "members_get_playlist.php?id_playlist={x0}&id_user={x1}";

        private static const VOTE:String = "members_set_vote.php";

        private static const GET_PLAYLISTS_LIST:String = "members.php?do=";

        public static const NEW_PLAYLISTS_LIST:String = "new";
        public static const ARCHIVED_PLAYLISTS_LIST:String = "archiw";

        public static function getPlaylistURL(playlistId:int, userId:int):String {
            return APP_URL + PLAYLISTS_DIR + prepare( GET_PLAYLIST, new <int>[playlistId, userId] );
        }

        private static function prepare(str:String, ...args):String{
            for ( var i:uint = 0; i < args[0].length; i++ ){
                str = str.replace( "{x" + i + "}", args[0][i] );
            }
            return str;
        }

        public static function getPlaylistsListURL(type:String):String{
            return APP_URL + GET_PLAYLISTS_LIST + type;
        }

    }
}
