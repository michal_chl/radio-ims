/**
 * Created by Michał Chlebowski on 2015-01-29.
 */
package pl.techcave.radioIMS.model {

import flash.events.EventDispatcher;

import pl.techcave.radioIMS.event.SoundsModelEvent;

import pl.techcave.radioIMS.model.vo.PlaylistVO;
import pl.techcave.radioIMS.model.vo.SoundVO;

    public class SoundsModel extends EventDispatcher {

        private static var _instance:SoundsModel;

        private var _soundsVOVector:Vector.<SoundVO> = new Vector.<SoundVO>;

        private var _playlistVector:Vector.<PlaylistVO> = new Vector.<PlaylistVO>;

        public function SoundsModel( lock:Lock ) {
        }

        public static function get instance():SoundsModel{
            if (!_instance) {
                _instance = new SoundsModel( new Lock() );
            }
            return _instance;
        }

        public function get soundsVOVector():Vector.<SoundVO> {
            return _soundsVOVector;
        }

        public function set soundsVOVector(value:Vector.<SoundVO>):void {
            _soundsVOVector = value;
        }

        public function get playlistVector():Vector.<PlaylistVO> {
            return _playlistVector;
        }

        public function set playlistVector(value:Vector.<PlaylistVO>):void {
            _playlistVector = value;
            dispatchEvent(new SoundsModelEvent( SoundsModelEvent.PLAYLISTS_COMPLETE ));
        }
    }
}

class Lock{
}

