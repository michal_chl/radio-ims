/**
 * Created by Michał Chlebowski on 2015-01-28.
 */
package pl.techcave.radioIMS.model.vo {
import flash.media.Sound;

public class SoundVO {

    private var _id:int;
    private var _link:String;
    private var _url:String;
    private var _title:String;
    private var _artist:String;
    private var _waga:int;
    private var _playlist:String;
    private var _sound:Sound;

    public function SoundVO(id:int, link:String, url:String, title:String, artist:String, waga:int, playlist:String) {
        _id = id;
        _link = link;
        _url = url;
        _title = title;
        _artist = artist;
        _waga = waga;
        _playlist = playlist;
    }

    public static function fromXML( value:XML ):SoundVO{
        return new SoundVO( parseInt( value.@id ), value.@link, value.@url, value.@title, value.@artist, parseInt( value.@waga ), value.@playlist );;
    }

    public function get id():int {
        return _id;
    }

    public function get link():String {
        return _link;
    }

    public function get url():String {
        return _url;
    }

    public function get title():String {
        return _title;
    }

    public function get artist():String {
        return _artist;
    }

    public function get waga():int {
        return _waga;
    }

    public function get playlist():String {
        return _playlist;
    }

    public function get sound():Sound {
        return _sound;
    }

    public function set sound(value:Sound):void {
        _sound = value;
    }
}
}
