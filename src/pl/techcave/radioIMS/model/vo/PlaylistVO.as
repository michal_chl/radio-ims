/**
 * Created by Michał Chlebowski on 2015-01-29.
 */
package pl.techcave.radioIMS.model.vo {
import com.greensock.loading.display.ContentDisplay;

public class PlaylistVO {

        private var _playlistId:int;

        private var _userId:int;

        private var _imageURL:String;

        private var _name:String;

        private var _image:ContentDisplay;

        public function PlaylistVO( playlistId:int, userId:int, imageURL:String, name:String ) {
            _playlistId = playlistId;
            _userId = userId;
            _imageURL = imageURL;
            _name = name;
        }

        public static function fromXML(value:XML):PlaylistVO{
            return new PlaylistVO( value.@playlistId, value.@userId, value.@imageURL, value.@name );
        }

        public function get playlistId():int {
            return _playlistId;
        }

        public function get userId():int {
            return _userId;
        }

        public function get imageURL():String {
            return _imageURL;
        }

        public function get name():String {
            return _name;
        }

    public function get image():ContentDisplay {
        return _image;
    }

    public function set image(value:ContentDisplay):void {
        _image = value;
    }
}

}
